﻿using System;

namespace Assignment1HelloYou
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter your name: ");
            string name = Console.ReadLine();

            int letterCount= 0;
            foreach(var letter in name)
            {
                letterCount++;
            }

            Console.WriteLine("Hello " + name + ". Your name contains " + letterCount + " letters and starts with " + name[0]);
        }
    }
}
